package com.sap.s4hana.controllers;

import com.sap.s4hana.services.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/campaign")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET })
public class CampaignController {
    private final CampaignService campaignService;

    @Autowired
    public CampaignController(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    @ResponseBody
    @RequestMapping(value = "/getCampaign", method = RequestMethod.GET)
    public String getCampaign(@RequestParam(value = "top") int top) {
        if(top <= 5000)
            return campaignService.getCampaign(top);
        else
            return "ERROR: top parameter must not be greater than 5000";
    }
}
