package com.sap.s4hana.controllers;

import com.sap.s4hana.services.CampaignMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/campaignMessage")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class CampaignMessageController {
    private final CampaignMessageService campaignMessageService;

    @Autowired
    public CampaignMessageController(CampaignMessageService campaignMessageService) {
        this.campaignMessageService = campaignMessageService;
    }

    @ResponseBody
    @RequestMapping(value = "/postCampaignMessage", method = RequestMethod.POST)
    public String postCampaignMessage() {
        return campaignMessageService.postMessage();
    }
}
