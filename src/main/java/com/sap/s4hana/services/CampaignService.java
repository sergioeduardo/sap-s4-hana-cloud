package com.sap.s4hana.services;

import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

@Service
public class CampaignService {
    public CampaignService() {}

    public String getCampaign(int top) {
        DataOutputStream dataOut = null;
        BufferedReader in = null;

        try {
            //API endpoint for API sandbox
            //String url = "https://sandbox.api.sap.com/s4hanacloud/sap/opu/odata/sap/API_MKT_CAMPAIGN_SRV;v=0002/Campaigns";
            //API endpoint with optional query parameters
            String url = "https://sandbox.api.sap.com/s4hanacloud/sap/opu/odata/sap/API_MKT_CAMPAIGN_SRV;v=0002/Campaigns?$top=" + top;
            //To view the complete list of query parameters, see its API definition.

            //Available API Endpoints
            //https://{host}:{port}/sap/opu/odata/sap/API_MKT_CAMPAIGN_SRV;v=0002

            URL urlObj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
            //setting request method
            connection.setRequestMethod("GET");

            //adding headers
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("Accept","application/json");
            //API Key for API Sandbox
            connection.setRequestProperty("APIKey","BNHjPwEpLTzS5eaFKAgeFqsdmiJWgDMh");

            //Available Security Schemes for productive API Endpoints
            //Basic Authentication

            //Basic Auth : provide username:password in Base64 encoded in Authorization header
            //connection.setRequestProperty("Authorization","Basic <Base64 encoded value>");

            connection.setDoInput(true);

            int responseCode = connection.getResponseCode();
            //in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            GZIPInputStream gzipInputStream = new GZIPInputStream(connection.getInputStream());
            Reader decoder = new InputStreamReader(gzipInputStream, "UTF-8");
            in = new BufferedReader(decoder);
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            //printing response
            System.out.println(response.toString());
            return response.toString();
        } catch (Exception e) {
            //do something with exception
            e.printStackTrace();
        } finally {
            try {
                if(dataOut != null) {
                    dataOut.close();
                }
                if(in != null) {
                    in.close();
                }
            } catch (IOException e) {
                //do something with exception
                e.printStackTrace();
            }
        }

        return "";
    }
}
