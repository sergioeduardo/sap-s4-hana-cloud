package com.sap.s4hana.services;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class CampaignMessageService {
    public CampaignMessageService() {}

    public String postMessage() {
        DataOutputStream dataOut = null;
        BufferedReader in = null;

        try {
            //API endpoint for API sandbox
            String url = "https://sandbox.api.sap.com/s4hanacloud/sap/opu/odata/sap/API_MKT_CAMPAIGN_MESSAGE_SRV/Messages";

            //Available API Endpoints (PROD URL)
            //https://{host}:{port}/sap/opu/odata/sap/API_MKT_CAMPAIGN_MESSAGE_SRV

            URL urlObj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
            //setting request method
            connection.setRequestMethod("POST");

            //adding headers
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("Accept","application/json");
            //connection.setRequestProperty("Content-Length", "100");
            //API Key for API Sandbox
            connection.setRequestProperty("APIKey","BNHjPwEpLTzS5eaFKAgeFqsdmiJWgDMh");


            //Available Security Schemes for productive API Endpoints
            //Basic Authentication

            //Basic Auth : provide username:password in Base64 encoded in Authorization header
            //connection.setRequestProperty("Authorization","Basic <Base64 encoded value>");

            connection.setDoInput(true);

            //sending POST request
            //connection.setFixedLengthStreamingMode(500);
            connection.setDoOutput(true);

            int responseCode = connection.getResponseCode();
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            //printing response
            System.out.println(response.toString());
            return response.toString();

        } catch (Exception e) {
            //do something with exception
            e.printStackTrace();
        } finally {
            try {
                if(dataOut != null) {
                    dataOut.close();
                }
                if(in != null) {
                    in.close();
                }

            } catch (IOException e) {
                //do something with exception
                e.printStackTrace();
            }
        }

        return "";
    }
}
