package com.sap.s4hana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S4hanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(S4hanaApplication.class, args);
	}
}
