FROM openjdk:8
ADD target/s4hana-0.0.1-SNAPSHOT.jar s4hana-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "s4hana-0.0.1-SNAPSHOT.jar"]